# Welcome to the RIDE

As soon as you open RIDE an editor screen such as this welcomes you.

![](images/openning_screen.png)

On the left hand side *Explorer* tab is selected by default. Here you can see current workspace directories and create new ones.

Workspace directory is a catkin workspace. Unter the src you can create new ros packages or install others from git explained in **Editing Project Configuration**

Above there is VS Code actions here you can create new files or directories or open terminal.

![](images/vs_code_actions.png)

At the bottom there is a tab with Riders Logo on it.

Here you can start/stop/reset simulation, build project, publish project, edit project configuration and stop the editor.

Under the *Nodes* tab you can see ROS nodes defined in **Project Configuration** and jump in them and start editing.

Pressing on **Publish Project** pushes every ROS package created under the src directory to Git. And when Editor is reopened created packages will also installed. So keep in mind to write your codes under the src folder

 ![](images/riders_actions.png)

* If you forked this project from another, pressing on **Build Project** reveals **Start** button at the top after building is done.

* If you created this project you need to add some configurations. **Editing Project Configuration** will be explained later.

Pressing Start launches simulation with given configuration. If there isn't an error with the packages simulation start correctly and simulation window should appear.

![](images/simulation.png)

As you can see actions under the simulation tabs are now changed and new *Sensors* tab appeared.

![](images/running_actions.png)

Under the *Sensors* tab you can see camera images published under rostopic.

* **View Simulator** button reveals simulator screen in case if it is closed.

* **Reset** button resets simulation and restarts all nodes.

* **Stop** button stops simulation.

### Editing Project Configuration

* Clicking on the **Edit configuration** reveals configuration of current project.

